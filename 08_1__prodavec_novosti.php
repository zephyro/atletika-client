<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>НОВОСТИ</h1>
                    </div>

                    <div class="post">
                        <a href="#" class="post__image">
                            <img src="images/news__01.jpg" class="img-fluid" alt="news">
                        </a>
                        <h3><a href="#">Комперссионный трикотаж со скидкой</a></h3>
                        <div class="post__intro">С другой стороны консультация с широким активом в значительной степени обуславливает создание позиций, занимаемых участниками в отношении поставленных задач. Разнообразный и богатый опыт сложившаяся структура организации влечет за собой процесс внедрения и модернизации модели развития. Таким образом рамки и место обучения кадров способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. С другой стороны укрепление и развитие структуры играет важную роль в формировании существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что дальнейшее развитие </div>
                        <div class="text-right">
                            <a href="#" class="post__view"><span>ПОДРОБНЕЙ</span></a>
                        </div>
                    </div>

                    <div class="post">
                        <a href="#" class="post__image">
                            <img src="images/news__02.jpg" class="img-fluid" alt="news">
                        </a>
                        <h3><a href="#">Комперссионный трикотаж со скидкой</a></h3>
                        <div class="post__intro">С другой стороны консультация с широким активом в значительной степени обуславливает создание позиций, занимаемых участниками в отношении поставленных задач. Разнообразный и богатый опыт сложившаяся структура организации влечет за собой процесс внедрения и модернизации модели развития. Таким образом рамки и место обучения кадров способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. С другой стороны укрепление и развитие структуры играет важную роль в формировани..</div>
                        <div class="text-right">
                            <a href="#" class="post__view"><span>ПОДРОБНЕЙ</span></a>
                        </div>
                    </div>

                    <div class="pagination">
                        <a  href="#" class="pagination_arrow disable"><i class="fas fa-angle-left"></i></a>
                        <ul>

                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">...</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                        </ul>
                        <a href="#" class="pagination_arrow"><i class="fas fa-angle-right"></i></a>
                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
