<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>КАБИНЕТ - МОИ Настройки</h1>
                    </div>

                    <div class="main__row">

                        <?php include ('inc/_sidebar.inc.php'); ?>

                        <section class="main__content">
                            <div class="white_box">

                                <form class="form mb_60">
                                    <div class="inline mb_20">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <h4>Общая информация</h4>
                                        </div>
                                    </div>
                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">e-MAIL</label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="text" class="form_control form_control_blue" name="email" placeholder="" value="alex928@gmail.com">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">Имя</label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="text" class="form_control form_control_blue" name="name" placeholder="Александр Пушков" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">Аптечная сеть</label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="text" class="form_control form_control_blue" name="name" placeholder="Александр Пушков" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">+79153254525</label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="text" class="form_control form_control_blue" name="name" placeholder="+7123456789" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">Адрес АПТЕКИ</label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="text" class="form_control" name="name" placeholder="" value="Москва, ул. Пушкинская д.32">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn">Сохранить новый адрес</button>
                                        </div>
                                    </div>
                                </form>

                                <form class="form">

                                    <div class="inline mb_20">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <h4>ИЗМЕНИТЬ ПАРОЛЬ</h4>
                                        </div>
                                    </div>

                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">тЕКУЩИЙ ПАРОЛЬ<span class="color_red">*</span></label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="password" class="form_control" name="name" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_divider"></div>

                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">Новый ПАРОЛЬ<span class="color_red">*</span></label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="password" class="form_control" name="name" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline mb_10">
                                        <div class="inline__left">
                                            <label class="form_label">ПОВТОРИТЬ ПАРОЛЬ<span class="color_red">*</span></label>
                                        </div>
                                        <div class="inline__right">
                                            <div class="input_wrap">
                                                <input type="password" class="form_control" name="name" placeholder="" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn">Сохранить новый пароль</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
