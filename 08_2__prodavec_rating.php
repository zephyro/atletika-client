<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__row">
                            <div class="heading__col">
                                <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                                <h1>РЕЙТИНГ</h1>
                                <div class="heading__info">
                                    Чем больше баллов - тем больше вероятность выиграть.<br/>
                                    Периодически мы проводим конкурсы
                                </div>
                            </div>
                            <div class="heading__col">
                                <div class="heading__rating">
                                    <strong>Я на <span class="color_purple">85</span> месте</strong>
                                    <span>Мне нужно набрать 150 баллов, чтобы войти в розыгрыш призов</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table_responsive">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Место</th>
                                <th>Имя</th>
                                <th>Количество баллов за текущий год</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td class="td_long">Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Алексей</td>
                                <td>35</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>


                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
