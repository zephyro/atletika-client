
$(".btn_modal").fancybox({
    'padding'    : 0
});

// SVG IE11 support
svg4everybody();

// Nav
(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $(this).toggleClass('open');
        $('.header__nav').toggleClass('open');
    });

}());

// Auth Tabs
(function() {

    $('.auth__nav a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.auth_tabs');

        $(this).closest('.auth__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.auth__tab').removeClass('active');
        box.find(tab).addClass('active');
    });

}());

(function() {

    $('.auth_switch__item').on('click touchstart', function(e){

        var tab = '#' + $(this).find('input').val();
        var box = $(this).closest('.auth__tab');

        box.find('.auth__type').removeClass('active');
        box.find(tab).addClass('active');
    });

}());