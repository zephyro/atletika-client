<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>КАБИНЕТ - МОИ ПРОДАЖИ</h1>
                    </div>

                    <div class="main__row">

                        <?php include ('inc/_sidebar.inc.php'); ?>

                        <section class="main__content">

                            <div class="content_heading">
                                <div class="heading__text">moi-email@mail.ru</div>
                                <div class="h1">КАБИНЕТ -  Мои подарочные КАРТЫ</div>
                            </div>

                            <div class="table_responsive mb_40">
                                <table class="table_strip">
                                    <thead>
                                    <tr>
                                        <th class="text_uppercase table_long text_left">название</th>
                                        <th class="text_uppercase">баллов</th>
                                        <th class="text_uppercase text-nowrap">Сумма в рублях</th>
                                        <th class="text_uppercase text-center">Дата</th>
                                        <th class="text_uppercase">Скачать</th>
                                    </tr>
                                    <tr>
                                        <th colspan="5"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text-nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>

                            <div class="pagination">
                                <a  href="#" class="pagination_arrow disable"><i class="fas fa-angle-left"></i></a>
                                <ul>

                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">...</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">16</a></li>
                                </ul>
                                <a href="#" class="pagination_arrow"><i class="fas fa-angle-right"></i></a>
                            </div>

                        </section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
