<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="main_wrap">

                        <h2 class="text-center">ВВЕДИТЕ КОД УКАЗАННЫЙ НА УПАКОВКЕ</h2>
                        <div class="elem">
                            <div class="elem__barcode">
                                <div class="elem__barcode_image">
                                    <img src="img/bar_code.png" class="img-fluid" alt="">
                                </div>
                                <div class="elem__barcode_help">Введите эти цифры</div>
                                <div class="elem__help">Введите эти цифры</div>
                            </div>
                            <div class="elem__input">
                                <input class="form_control text-center" type="text" name="code">
                            </div>
                            <div class="elem__error"><span>такого кода не существует</span></div>
                            <div class="text-center">
                                <button type="submit" class="btn">Добавить покупку</button>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
