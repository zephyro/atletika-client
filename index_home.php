<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>МОИ ПРОДАВЦЫ</h1>
                    </div>

                    <div class="main_row">

                        <aside class="sidebar">
                            <nav class="sidenav">
                                <ul>
                                    <li class="active"><a href="#">Мои продажи</a></li>
                                    <li class="active"><a href="#">Мои подарочне карты</a></li>
                                    <li class="active"><a href="#">Мои настройки</a></li>
                                    <li class="sidenav_exit"><a href="#">ВЫХОД</a></li>
                                </ul>
                            </nav>
                        </aside>

                        <section class="content"></section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
