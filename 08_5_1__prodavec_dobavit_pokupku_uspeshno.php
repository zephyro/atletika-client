<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="main_wrap">

                        <h2 class="text-center">ВВЕДИТЕ КОД УКАЗАННЫЙ НА УПАКОВКЕ</h2>
                        <div class="elem">
                            <div class="elem__ring">
                                <div class="elem__ring_wrap">
                                    Вам начислено<br/>
                                    <span class="color_purple">80</span> баллов
                                </div>
                            </div>
                            <div class="elem__name">
                                ТОВАР<br/>
                                “Подпяточник Shoeboys Ortho Heel”
                            </div>


                            <div class="text-center mb_40">
                                <button type="submit" class="btn btn_lg">Добавить покупку</button>
                            </div>

                            <div class="text-center">
                                <a class="elem__link" href="#">перейти в личный кабинет</a>
                            </div>

                        </div>

                    </div>

                </div>
            </section>


            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
