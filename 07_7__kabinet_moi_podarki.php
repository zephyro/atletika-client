<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>КАБИНЕТ -  Мои Настройки</h1>
                    </div>

                    <div class="main__row">

                        <aside class="main__sidebar">

                            <nav class="sidenav">
                                <ul>
                                    <li class="active"><a href="#">Мои покупки</a></li>
                                    <li><a href="#">Мои подарки</a></li>
                                    <li><a href="#">Мои настройки</a></li>
                                    <li class="sidenav_exit"><a href="#">ВЫХОД</a></li>
                                </ul>
                            </nav>

                            <div class="side_news">
                                <div class="side_news__heading">Последние новости</div>
                                <ul>
                                    <li>
                                        <a href="#">Новые скидки на компрессионные чулки уже сейчас</a>
                                        <span> - 21.12.2019</span>
                                    </li>
                                    <li>
                                        <a href="#">Новые скидки на компрессионные чулки </a>
                                        <span> - 21.12.2019</span>
                                    </li>
                                    <li>
                                        <a href="#">Новые скидки на компрессионные чулки </a>
                                        <span> - 21.12.2019</span>
                                    </li>
                                </ul>
                            </div>

                        </aside>

                        <section class="main__content">

                            <div class="table_responsive mb_40">
                                <table class="table_strip">
                                    <thead>
                                    <tr>
                                        <th class="text_uppercase table_long text_left">Название</th>
                                        <th class="text_uppercase">Списано Баллов</th>
                                        <th class="text_uppercase text-nowrap">Дата</th>
                                    </tr>
                                    <tr>
                                        <th colspan="5"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase"><strong>За январь</strong></td>
                                        <td>32</td>
                                        <td class="text-nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>

                        </section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
