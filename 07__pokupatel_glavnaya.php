<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>КАБИНЕТ - МОИ ПРОДАЖИ</h1>
                    </div>

                    <div class="main__row">

                        <?php include ('inc/_sidebar.inc.php'); ?>

                        <section class="main__content">

                            <div class="message">
                                <div class="message__icon color_dark_blue">!</div>
                                <div class="message__text">Специализированное сообщение для покупателя</div>
                            </div>

                            <div class="row mb_20">
                                <div class="col col-xs-12 col-sm-4">
                                    <div class="widget">
                                        <div class="widget_actions">
                                            <img src="img/wighet_actions.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="widget_actions hide">
                                            <div class="widget_actions__exchange">1 балл = 1р.</div>
                                            <div class="widget_actions__word">ПОЛУЧАЙТЕ</div>
                                            <div class="widget_actions__row">
                                                <div class="widget_actions__word">БАЛЛЫ</div>
                                                <div class="widget_actions__text">за каждую <br/>продажу</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-4">
                                    <div class="widget">
                                        <div class="widget_info">
                                            <div class="widget_info__heading">БАЛЛЫ ПОЗВОЛЯЮТ</div>
                                            <div class="mb_10">- получать подарки из каталога компрессионного трикотажа</div>
                                            <div class="mb_10">- подарки из нашего ассортимента</div>
                                            <a href="#">условия использования</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-4">
                                    <div class="widget">
                                        <div class="widget_balance">
                                            <div class="widget_balance__heading">Мой баланс</div>
                                            <div class="widget_balance__value"><strong><span class="color_purple">256</span> баллов</strong></div>
                                            <div class="mb_10">Раз в 3 месяца мы проводим розыгрыш среди наших покупаетелей</div>
                                            <div class="mb_10 color_black">50 баллов - средство для стирки компрессионного трикотажа или массажный мячик (на усмотрение администрации)</div>
                                            <div class="mb_10 color_black">Общее количество набранных баллов за год больше 1000 - участие в розыгрыше поездки в Италию</div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                            <h2 class="color_purple">ТРИКОТАЖ СЕЙЧАС В РОЗНИЦЕ</h2>

                            <div class="map_search mb_20">
                                <div class="map_search__elem map_search__place">
                                    <select class="form_control form_control_lg form_select" name="city">
                                        <option value="">Выбрать город</option>
                                        <option value="">Москва</option>
                                        <option value="">Новгород</option>
                                        <option value="">Рязань</option>
                                        <option value="">Волгоград</option>
                                        <option value="">Новосибирск</option>
                                    </select>
                                </div>
                                <div class="map_search__elem map_search__text">
                                    <div class="map_search__custom">
                                        <input class="form_control form_control_lg" type="text" name="name" value="" placeholder="Россия, Москва, улица Зеленодольская">
                                        <button class="map_search__btn" type="submit"></button>
                                    </div>
                                </div>
                                <div class="map_search__elem map_search__filter">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" value="" checked>
                                        <span>Со скидкой</span>
                                    </label>
                                </div>
                            </div>

                            <div class="map mb_40" id="map">

                            </div>

                            <div class="table_responsive mb_40">
                                <table class="table_strip">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th class="text-center">Баллов</th>
                                        <th class="text-center">Дата</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>ЗА ИЗУЧЕНИЕ МАТЕРИАЛА “НАЗВАНИЕ СТАТЬЯ”</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="pagination">
                                <a  href="#" class="pagination_arrow disable"><i class="fas fa-angle-left"></i></a>
                                <ul>

                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">...</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">16</a></li>
                                </ul>
                                <a href="#" class="pagination_arrow"><i class="fas fa-angle-right"></i></a>
                            </div>

                        </section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

        <!-- Map -->
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script>

            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map('map', {
                        center: [55.76, 37.64],
                        zoom: 10,
                        controls: ['zoomControl']
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    objectManager = new ymaps.ObjectManager();

                myMap.geoObjects.add(objectManager);

                $.ajax({
                    // В файле data.json заданы геометрия, опции и данные меток .
                    url: "data.json"
                }).done(function(data) {
                    objectManager.add(data);
                });

            }

        </script>

    </body>
</html>
