<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="auth">
            <div class="container">

                <a href="/" class="auth__logo">
                    <img src="img/auth__logo.svg" class="img-fluid" alt="">
                </a>

                <div class="auth__heading">
                    <h1>Бонусная программа</h1>
                    <span>по компрессионному трИкотажу ERGOFORMA</span>
                </div>

                <div class="auth__content auth_tabs">

                    <div class="auth__tab mb_40 active" id="confirmation">
                        <div class="auth__message">
                            <div class="auth__message_wrap">
                                <div class="auth__message_title">ПОДТВЕРДИТЕ СВОЮ ПОЧТУ</div>
                                <div class="auth__message_text">Перейдите по ссылке в письме</div>
                            </div>
                        </div>
                    </div>

                    <div class="auth__apps">
                        <a href="#">
                            <img src="img/icon__app_store.png" alt="">
                        </a>
                        <a href="#">
                            <img src="img/icon__google_play.png" alt="">
                        </a>
                    </div>

                </div>

            </div>
        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
