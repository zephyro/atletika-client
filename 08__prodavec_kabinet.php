<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>КАБИНЕТ - МОИ ПРОДАЖИ</h1>
                    </div>

                    <div class="main__row">

                        <?php include ('inc/_sidebar.inc.php'); ?>

                        <section class="main__content">

                            <div class="message">
                                <div class="message__icon color_purple">!</div>
                                <div class="message__text">Специализированное сообщение для продавца</div>
                            </div>

                            <div class="row mb_20">
                                <div class="col col-xs-12 col-sm-4">
                                    <div class="widget">
                                        <div class="widget_actions">
                                            <img src="img/wighet_actions.svg" class="img-fluid" alt="">
                                        </div>
                                        <div class="widget_actions hide">
                                            <div class="widget_actions__exchange">1 балл = 1р.</div>
                                            <div class="widget_actions__word">ПОЛУЧАЙТЕ</div>
                                            <div class="widget_actions__row">
                                                <div class="widget_actions__word">БАЛЛЫ</div>
                                                <div class="widget_actions__text">за каждую <br/>продажу</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-4">
                                    <div class="widget">
                                        <div class="widget_info">
                                            <div class="widget_info__heading">ПОЛУЧАЙТЕ БАЛЛЫ</div>
                                            <div class="widget_info__plus">+10 баллов за изучение</div>
                                            <ul>
                                                <li><a href="#">Информация для изучения 1</a></li>
                                                <li><a href="#">Информация для изучения 2</a></li>
                                                <li><a href="#">Информация для изучения 3</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-4">
                                    <div class="widget">
                                        <div class="widget_balance">
                                            <div class="widget_balance__heading">Мой баланс</div>
                                            <div class="widget_balance__value"><strong><span class="color_purple">256</span> баллов</strong><sup>256р.</sup></div>
                                            <div class="mb_10">Переодически мы меняем Ваши баллы на <a href="#">подарочные сертификаты</a></div>
                                            <div>Общее количество баллов в конкурсе 832 балла мне нужно 168 баллов, чтобы попасть розыгрыш поездки в Италию.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="content_heading">
                                <div class="heading__text">всего <span class="color_purple">продано на 12256</span> баллов</div>
                                <div class="h1">МОИ ПРОДАЖИ</div>
                            </div>

                            <div class="table_responsive mb_40">
                                <table class="table_strip">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th class="text-center">Баллов</th>
                                        <th class="text-center">Дата</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>ЗА ИЗУЧЕНИЕ МАТЕРИАЛА “НАЗВАНИЕ СТАТЬЯ”</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Компрессионные чулки #202</strong>
                                        </td>
                                        <td class="text-nowrap text-center">10</td>
                                        <td class="text-nowrap text-center">11.12.2019,11:26</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="pagination">
                                <a  href="#" class="pagination_arrow disable"><i class="fas fa-angle-left"></i></a>
                                <ul>

                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">...</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">16</a></li>
                                </ul>
                                <a href="#" class="pagination_arrow"><i class="fas fa-angle-right"></i></a>
                            </div>

                        </section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
