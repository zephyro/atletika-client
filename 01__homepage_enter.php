<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="auth">
            <div class="container">

                <a href="/" class="auth__logo">
                    <img src="img/auth__logo.svg" class="img-fluid" alt="">
                </a>

                <div class="auth__heading">
                    <h1>Бонусная программа</h1>
                    <span>по компрессионному трИкотажу ERGOFORMA</span>
                </div>

                <div class="auth__content auth_tabs">

                    <div class="auth__nav">
                        <a class="active" href="#enter"><span>Вход</span></a>
                        <a href="#reg"><span>Регистрация</span></a>
                    </div>

                    <div class="auth__tab mb_30 active" id="enter">

                        <form class="form">

                            <div class="form_box">
                                <div class="form_box__icon">
                                    <div class="form_box__icon_wrap">
                                        <i>
                                            <img src="img/icon__email.svg" class="img-fluid" alt="">
                                        </i>
                                    </div>
                                </div>
                                <div class="form_box__content">
                                    <input class="form_box__control" type="text" name="email" placeholder="Email">
                                </div>
                            </div>

                            <div class="form_box mb_50">
                                <div class="form_box__icon">
                                    <div class="form_box__icon_wrap">
                                        <i>
                                            <img src="img/icon__password.svg" class="img-fluid" alt="">
                                        </i>
                                    </div>
                                </div>
                                <div class="form_box__content">
                                    <input class="form_box__control" type="password" name="name" placeholder="Пароль">
                                </div>
                            </div>

                            <div class="text-center mb_40">
                                <button type="submit" class="btn btn_light_blue btn_long text_uppercase text_strong">Войти</button>
                            </div>
                            <div class="text-center">
                                <a class="auth_recovery auth_link" href="#recovery">Восстановить пароль</a>
                            </div>

                        </form>

                    </div>

                    <div class="auth__tab mb_40" id="reg">

                        <div class="auth_switch">
                            <label class="auth_switch__item">
                                <input type="radio" name="auth_type" value="buyer" checked>
                                <span>Я Покупатель</span>
                            </label>
                            <label class="auth_switch__item">
                                <input type="radio" name="auth_type" value="seller">
                                <span>Я Продавец</span>
                            </label>
                        </div>

                        <div class="auth__type active" id="buyer">
                            <form class="form">

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__user.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="text" name="name" placeholder="Имя">
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__email.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="text" name="email" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__address.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <div class="form_box__label">
                                            <strong>Почтовый адрес для подарков</strong>
                                            <span>Указывайте точный адрес с Ф.И.О.</span>
                                        </div>
                                        <textarea class="form_box__control" name="address" placeholder="Пример: Московская область, г. Москва, ул. Пушкина 32-123, индекс 000912, Иванов Иван Иванович" rows="4"></textarea>
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__password.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="password" name="name" placeholder="Пароль">
                                    </div>
                                </div>

                                <div class="form_box mb_50">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__password_repeat.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="password" name="name" placeholder="Повторите пароль">
                                    </div>
                                </div>

                                <div class="form_box__check mb_40">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span><a href="#">Подтверждаю согласие с условиями</a></span>
                                    </label>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn_light_blue btn_long text_uppercase text_strong">Войти</button>
                                </div>
                            </form>
                        </div>

                        <div class="auth__type" id="seller">
                            <form class="form">

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__user.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="text" name="name" placeholder="Имя">
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__phone.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="text" name="phone" placeholder="Телефон">
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__place.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <div class="form_box__label">
                                            <strong>Адрес розничной точки</strong>
                                        </div>
                                        <textarea class="form_box__control" name="address" placeholder="Москва, ул. Пушкина 32" rows="4"></textarea>
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__pharm.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <div class="form_box__wrap">
                                            <select class="form_box__control form_box__select" name="net">
                                                <option value="Аптечная сеть">Аптечная сеть</option>
                                                <option value="Аптечная сеть">Аптечная сеть</option>
                                                <option value="Аптечная сеть">Аптечная сеть</option>
                                                <option value="Аптечная сеть">Аптечная сеть</option>
                                                <option value="Аптечная сеть">Аптечная сеть</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__email.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="text" name="email" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form_box">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__password.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="password" name="name" placeholder="Пароль">
                                    </div>
                                </div>

                                <div class="form_box mb_50">
                                    <div class="form_box__icon">
                                        <div class="form_box__icon_wrap">
                                            <i>
                                                <img src="img/icon__password_repeat.svg" class="img-fluid" alt="">
                                                <span class="color_purple">*</span>
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form_box__content">
                                        <input class="form_box__control" type="password" name="name" placeholder="Повторите пароль">
                                    </div>
                                </div>

                                <div class="form_box__check mb_40">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="check" checked>
                                        <span><a href="#">Подтверждаю согласие с условиями</a></span>
                                    </label>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn_light_blue btn_long text_uppercase text_strong">Войти</button>
                                </div>

                            </form>
                        </div>
                        
                    </div>

                    <div class="auth__tab mb_40" id="recovery">
                        <div class="auth__title">Восстановление пароля</div>
                        <form class="form">
                            <div class="form_box mb_40">
                                <div class="form_box__icon">
                                    <div class="form_box__icon_wrap">
                                        <i>
                                            <img src="img/icon__email.svg" class="img-fluid" alt="">
                                        </i>
                                    </div>
                                </div>
                                <div class="form_box__content">
                                    <input class="form_box__control" type="text" name="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn_light_blue btn_long text_uppercase text_strong">Восстановить</button>
                            </div>
                        </form>
                    </div>

                    <div class="auth__tab mb_40" id="password">
                        <div class="auth__title">Введите новый пароль</div>
                        <form class="form">
                            <div class="form_box">
                                <div class="form_box__icon">
                                    <div class="form_box__icon_wrap">
                                        <i>
                                            <img src="img/icon__password.svg" class="img-fluid" alt="">
                                            <span class="color_purple">*</span>
                                        </i>
                                    </div>
                                </div>
                                <div class="form_box__content">
                                    <input class="form_box__control" type="password" name="name" placeholder="Пароль">
                                </div>
                            </div>

                            <div class="form_box mb_40">
                                <div class="form_box__icon">
                                    <div class="form_box__icon_wrap">
                                        <i>
                                            <img src="img/icon__password_repeat.svg" class="img-fluid" alt="">
                                            <span class="color_purple">*</span>
                                        </i>
                                    </div>
                                </div>
                                <div class="form_box__content">
                                    <input class="form_box__control" type="password" name="name" placeholder="Повторите пароль">
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn_light_blue btn_long text_uppercase text_strong">сохранить пароль</button>
                            </div>
                        </form>
                    </div>

                    <div class="auth__apps">
                        <a href="#">
                            <img src="img/icon__app_store.png" alt="">
                        </a>
                        <a href="#">
                            <img src="img/icon__google_play.png" alt="">
                        </a>
                    </div>

                </div>

            </div>
        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
