
<div class="topbar">
    <div class="container">
        <div class="topbar__row">
            <div class="topbar__name">БОНУСНАЯ ПРОГРАММА</div>
            <div class="topbar__text">Я продавец | Аптечная сеть  ООО “Аптечная сеть”, адрес г.Москва ул. Куприянова 21</div>
        </div>
    </div>
</div>

<header class="header">
    <div class="container">
        <div class="header__row">
            <a href="/" class="header__logo">
                <i><img src="img/logo.svg" class="img-fluid" alt=""></i>
                <span>Решает, украшает, привлекает!</span>
            </a>

            <nav class="header__nav">
                <ul>
                    <li>
                        <span class="header__nav_rating">я <strong class="color_purple">85</strong> в рейтинге</span>
                        <a href="#"><span>Рейтинг</span></a>
                    </li>
                    <li><a href="#"><span>Новости</span></a></li>
                    <li class="active"><a href="#"><span>Кабинет</span></a></li>
                </ul>
                <a class="btn btn_shadow"><span>Добавить продажу</span></a>
            </nav>

            <a class="header__toggle nav_toggle" href="#">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
    </div>
</header>
