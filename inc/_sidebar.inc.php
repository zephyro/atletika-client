<aside class="main__sidebar">

    <nav class="sidenav">
        <ul>
            <li class="active"><a href="#">Мои продажи</a></li>
            <li><a href="#">Мои подарочные карты</a></li>
            <li><a href="#">Мои настройки</a></li>
            <li class="sidenav_exit"><a href="#">ВЫХОД</a></li>
        </ul>
    </nav>

    <div class="side_news">
        <div class="side_news__heading">Последние новости</div>
        <ul>
            <li>
                <a href="#">Новые скидки на компрессионные чулки уже сейчас</a>
                <span> - 21.12.2019</span>
            </li>
            <li>
                <a href="#">Новые скидки на компрессионные чулки </a>
                <span> - 21.12.2019</span>
            </li>
            <li>
                <a href="#">Новые скидки на компрессионные чулки </a>
                <span> - 21.12.2019</span>
            </li>
        </ul>
    </div>

</aside>