<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__content">
                <a href="/" class="footer__logo">
                    <img src="img/footer__logo.svg" class="img-fluid" alt="">
                </a>
                <div class="footer__text">
                    <p>ООО «БМК»</p>
                    <p>
                        Общество с ограниченной ответственностью «Биомеханика»<br/>
                        Юр. Адрес/факт.адрес:<br/>
                        198095, Санкт-Петербург, ул. Швецова д.23, лит.Б. пом. 6-Н<br/>
                        Тел./факс: (812)740-70-68, (812)740-70-66<br/>
                        ИНН 7805327562, КПП780501001, ОГРН 1157847345523<br/>
                        ОКПО 49900190, ОКАТО 40276565000<br/>
                    </p>
                    <a href="#">Условия использования сайта</a>
                </div>
            </div>
            <div class="footer__store">
                <a href="#" class="footer__store_app"></a>
                <a href="#" class="footer__store_google"></a>
            </div>

        </div>
    </div>
</footer>

<div class="bottom_line">
    <div class="container">
        <div class="bottom_line__row">
            <span>Все права защищены </span>
            <a href="#">Разработка Hix.one</a>
        </div>
    </div>
</div>