<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <div class="topbar topbar_client">
                <div class="container">
                    <div class="topbar__row">
                        <div class="topbar__name">БОНУСНАЯ ПРОГРАММА</div>
                        <div class="topbar__text">Я клиент | Аптечная сеть ООО “Аптечная сеть”</div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="/" class="header__logo">
                            <i><img src="img/logo.svg" class="img-fluid" alt=""></i>
                            <span>Решает, украшает, привлекает!</span>
                        </a>

                        <nav class="header__nav">
                            <ul>
                                <li class="active"><a href="#"><span>Кабинет</span></a></li>
                            </ul>
                        </nav>

                        <a class="header__toggle nav_toggle" href="#">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
            </header>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__text"><a href="#">moi-email@mail.ru</a></div>
                        <h1>МОИ ПРОДАВЦЫ</h1>
                    </div>

                    <div class="main__row">

                        <aside class="main__sidebar">
                            <nav class="sidenav text-right">
                                <ul>
                                    <li class="active"><a href="#">Мои продавцы</a></li>
                                    <li class="sidenav_exit"><a href="#">ВЫХОД</a></li>
                                </ul>
                            </nav>
                        </aside>

                        <section class="main__content">

                            <div class="table_sort">
                                <div class="table_sort__item">
                                    <div class="table_sort__label">Дата от</div>
                                    <div class="table_sort__input">
                                        <input type="text" class="form_control form_control_sm" name="name" placeholder="">
                                    </div>
                                </div>
                                <div class="table_sort__item">
                                    <div class="table_sort__label">до</div>
                                    <div class="table_sort__input">
                                        <input type="text" class="form_control form_control_sm" name="name" placeholder="">
                                    </div>
                                </div>
                                <div class="table_sort__item">
                                    <a href="#">показать данные за этот период</a>
                                </div>
                            </div>

                            <div class="table_responsive">

                                <table class="table_light">

                                    <thead>

                                    <tr>
                                        <th>Имя</th>
                                        <th>Дата регистрации </th>
                                        <th>Адрес розничной точки </th>
                                        <th>Сколько продали</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4"></th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>

                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>

                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td>Алексей</td>
                                        <td class="text-nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>

                                    </tbody>

                                </table>

                            </div>

                        </section>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

            <?php include ('inc/modal.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
